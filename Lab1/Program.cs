using System;
namespace lab1
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine()!;
            double xMin = Double.Parse(sxMin);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine()!;
            double xMax = double.Parse(sxMax);
            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine()!;
            double dx = double.Parse(sdx);

            double x = xMin;
            double y;

            double sinSum = 0;

            while (x <= xMax)
            {
                double x1 = x;
                double x2 = 3 * x;

                double sqrt = Math.Sqrt(x2 / (x1 + Math.Pow(53 * x2, 2)));

                y = Program.Cos4(x1 - sqrt);

                sinSum += Math.Sin(y);

                Console.WriteLine("x = {0}\t\t y = {1}", x, y);

                x += dx;
            }

            Console.WriteLine("Сума синусів: {0}", sinSum);
        }

        public static double Cos4(double x)
        {
            // https://proofwiki.org/wiki/Power_Reduction_Formulas/Cosine_to_4th
            // cos4 formula
            // 3 + 4cos2x + cox4x
            // -------------------
            //          8

            double top = 3 +
                4 * Math.Cos(2 * x) +
                Math.Cos(4 * x);

            double bottom = 8;

            return top / bottom;
        }


    }
}


