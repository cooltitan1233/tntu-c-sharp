﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb8
{
    public class Country
    {
        public string Name;
        public string Region;
        public int Population;
        public double YearIncome;
        public double Square;
        public bool HasYadernaZbroia;
        public bool HasChlenNato;
        public double GetYearIncomePerInhabitant()
        {
            return YearIncome / Population;
        }
    }
}
