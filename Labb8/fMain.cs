﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labb8
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnAddCountry_Click(object sender, EventArgs e)
        {
            Country country = new Country();
            fCountry ft = new fCountry(country);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                String slave = string.Format(@"{0},{1}. Мешканців: {2}. Річний дохід:{3:0.00} грн.Площа: {4:0.000}кв.км. [{5} | {6}] | Річний дохід намешканця: {7:0.00} грн 
", country.Name, country.Region, country.Population, country.YearIncome, country.Square, country.HasYadernaZbroia ? "Є ядерна зброя" : "Немає ядерної зброї", country.HasChlenNato ? "Країна є членом НАТО" : "Країна не член НАТО", country.GetYearIncomePerInhabitant());

                tbCountriesInfo.Text = slave;
                //tbCountriesInfo.Text += string.Format("{1},{2}. Мешканців: {3}. Річний дохід:{ 4:0.00} грн.Площа: {5:0.000}кв.км. [{6} | {7}] | Річний дохід намешканця: {8:0.00} грн\r\n", country.Name, country.Region, country.Population, country.YearIncome, country.Square, country.HasYadernaZbroia ? "Є ядерна зброя" : "Немає ядерної зброї", country.HasChlenNato ? "Країна є членом НАТО" : "Країна не член НАТО", country.GetYearIncomePerInhabitant());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?", "Припинити роботу", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }

        private void tbCountriesInfo_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
