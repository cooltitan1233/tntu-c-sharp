﻿
namespace Labb8
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCountriesInfo = new System.Windows.Forms.TextBox();
            this.btnAddCountry = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbCountriesInfo
            // 
            this.tbCountriesInfo.Location = new System.Drawing.Point(12, 12);
            this.tbCountriesInfo.Multiline = true;
            this.tbCountriesInfo.Name = "tbCountriesInfo";
            this.tbCountriesInfo.ReadOnly = true;
            this.tbCountriesInfo.Size = new System.Drawing.Size(962, 674);
            this.tbCountriesInfo.TabIndex = 0;
            this.tbCountriesInfo.TextChanged += new System.EventHandler(this.tbCountriesInfo_TextChanged);
            // 
            // btnAddCountry
            // 
            this.btnAddCountry.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddCountry.Location = new System.Drawing.Point(1041, 12);
            this.btnAddCountry.Name = "btnAddCountry";
            this.btnAddCountry.Size = new System.Drawing.Size(218, 80);
            this.btnAddCountry.TabIndex = 1;
            this.btnAddCountry.Text = "Додати країну";
            this.btnAddCountry.UseVisualStyleBackColor = true;
            this.btnAddCountry.Click += new System.EventHandler(this.btnAddCountry_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClose.Location = new System.Drawing.Point(1041, 596);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(218, 80);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1319, 698);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAddCountry);
            this.Controls.Add(this.tbCountriesInfo);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "\"Лабораторна робота №8\"";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbCountriesInfo;
        private System.Windows.Forms.Button btnAddCountry;
        private System.Windows.Forms.Button btnClose;
    }
}

