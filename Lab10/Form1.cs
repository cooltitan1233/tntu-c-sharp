﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми",
            MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvCountries.AutoGenerateColumns = false;

            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Region";
            column.Name = "Регіон";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Population";
            column.Name = "Мешканців";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "YearIncome";
            column.Name = "Річн. дохід";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Square";
            column.Name = "Площа";
            column.Width = 80;
            gvCountries.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "HasYadernazbroia";
            column.Name = "Ядерна зброя";
            column.Width = 60;
            gvCountries.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "HasChlennato";
            column.Name = "Країна член НАТО";
            column.Width = 60;
            gvCountries.Columns.Add(column);

            bindSrcCountries.Add(new Country("Україна", "Європа", 800000, 2000000, 400, false, true));
            EventArgs args = new EventArgs();
            OnResize(args);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Country country = new Country();
            fCountry ft = new fCountry(country);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcCountries.Add(country);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Country country = (Country)bindSrcCountries.List[bindSrcCountries.Position];
            fCountry ft = new fCountry(country);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcCountries.List[bindSrcCountries.Position] = country;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?",
            "Видалення запису", MessageBoxButtons.OKCancel,
             MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindSrcCountries.RemoveCurrent();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені",
          "Очищення даних", MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindSrcCountries.Clear();
            }
        }
    }
}
