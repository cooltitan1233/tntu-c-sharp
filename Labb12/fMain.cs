﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labb12
{
    public partial class fMain : Form
    {
        CCircle[] circles;
        int CircleCount = 0;
        int CurrentCircleIndex;
        CF[] figures;
        int FiguresCount = 0;
        int CurrentFigureIndex;
        public fMain()
        {
            InitializeComponent();

            circles = new CCircle[100];
            figures = new CF[100];
            cbFigureType.SelectedIndex = 0;
        }

        private void pnMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void fMain_Load(object sender, EventArgs e)
        {


        }

        private void btnCreateNew_Click(object sender, EventArgs e)
        {
            if (CircleCount >= 99) { MessageBox.Show("Досягнуто межі кількості об'єктів!"); return; }
            Graphics graphics = pnMain.CreateGraphics(); CurrentCircleIndex = CircleCount;


            circles[CurrentCircleIndex] =
                new CCircle(graphics, pnMain.Width / 2,
                pnMain.Height / 2, 50);
            circles[CurrentCircleIndex].Show();
            CircleCount++;

            cbCircles.Items.Add("Коло №" + (CircleCount - 1).ToString());
            cbCircles.SelectedIndex = CircleCount - 1;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;


            figures[CurrentFigureIndex].Hide();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;


            figures[CurrentFigureIndex].Show();
        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;


            figures[CurrentFigureIndex].Expand(5);
        }

        private void btnCollapse_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Collapse(5);
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(0, -10);
        }

        private void btnUpFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++) { figures[CurrentFigureIndex].Move(0, -1); System.Threading.Thread.Sleep(5); }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(0, 10);
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(10, 0);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(-10, 0);
        }

        private void btnRightFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++) { figures[CurrentFigureIndex].Move(1, 0); System.Threading.Thread.Sleep(5); }
        }

        private void btnLeftFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++) { figures[CurrentFigureIndex].Move(-1, 0); System.Threading.Thread.Sleep(5); }
        }

        private void btnDownFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbCircles.SelectedIndex; if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++) { figures[CurrentFigureIndex].Move(0, 1); System.Threading.Thread.Sleep(5); }
        }

        private void pnTools_Paint(object sender, PaintEventArgs e)
        {

        }
        private void cbFigureType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (FiguresCount >= 99) { MessageBox.Show("Досягнуто максимальної кількості об'єктів!"); return; }
            Graphics graphics = pnMain.CreateGraphics();
            CurrentFigureIndex = FiguresCount;



            if (cbFigureType.SelectedIndex == 0)
            {
                figures[CurrentFigureIndex] = new CEmblem(graphics, pnMain.Width / 4, pnMain.Height / 2, 50);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() + " [коло]");
                figures[CurrentFigureIndex].Show();

            }
            else if (cbFigureType.SelectedIndex == 1)
            {
                figures[CurrentFigureIndex] = new CR(graphics, pnMain.Width / 2, pnMain.Height / 2, 100, 50);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() + " [прямокутник]");
                figures[CurrentFigureIndex].Show();
            }
            else if (cbFigureType.SelectedIndex == 2)
            {
                figures[CurrentFigureIndex] = new CT(graphics, pnMain.Width / 2, pnMain.Height / 2, 100);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() + " [трикутник]");
                figures[CurrentFigureIndex].Show();
            }
            else if (cbFigureType.SelectedIndex == 3)
            {

                figures[1] = new CEmblem(graphics, pnMain.Width / 3, pnMain.Height / 2, 50);

                figures[2] = new CR(graphics, pnMain.Width / 2, pnMain.Height / 2, 50, 50);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() + " [емблема]");
                figures[3] = new CT(graphics, pnMain.Width / 8, pnMain.Height / 2, 100);
                cbCircles.Items.Add("Фігура №" + (FiguresCount).ToString() + " [емблема]");
                figures[1].Show();
                figures[2].Show();
                figures[3].Show();
            }

            FiguresCount++; cbCircles.SelectedIndex = FiguresCount - 1;
        }
    }
}
