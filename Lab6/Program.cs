﻿using System;
namespace Lab6
{
    class Program
    {
        class Country
        {
            public string Name;
            public string Region;
            public int Population; public double YearIncome;
            public double Square;
            public bool HasYadernazbroia;
            public bool HasChlennato;
            public double YearIncomePerInhabitant
            {
                get
                {
                    return GetYearIncomePerInhabitant();
                }
            }
            public double GetYearIncomePerInhabitant()
            {
                return YearIncome / Population;
            }

        }


        static void Main(string[] args)
        {
            Country[] arrCountries;
            Console.Write("Введiть кiлькiсть країн: ");
            int cntCountries = int.Parse(Console.ReadLine());
            arrCountries = new Country[cntCountries];
            for (int i = 0; i < cntCountries; i++)
            {
                Console.Write("Введiть назву країни: ");
                string sName = Console.ReadLine();
                Console.Write("Введiть назву частини світу: ");
                string sRegion = Console.ReadLine();
                Console.Write("Введiть кiлькiсть населення: ");
                string sPopulation = Console.ReadLine();
                Console.Write("Введiть рiчний дохiд: ");
                string sYearIncome = Console.ReadLine();
                Console.Write("Введiть площу, кв. км: ");
                string sSquare = Console.ReadLine();
                Console.Write("Чи є у країни ядерна зброя ? (y-так, n-нi): ");
                ConsoleKeyInfo keyHasYadernazbroia = Console.ReadKey();
                Console.WriteLine();
                Console.Write("Чи є країна членом НАТО? (y-так, n-нi): ");
                ConsoleKeyInfo keyHasChlennato = Console.ReadKey();
                Console.WriteLine();
                Country OurCountry = new Country();
                OurCountry.Name = sName;
                OurCountry.Region = sRegion;
                OurCountry.Population = int.Parse(sPopulation);
                OurCountry.YearIncome = double.Parse(sYearIncome);
                OurCountry.Square = double.Parse(sSquare);
                OurCountry.HasYadernazbroia = keyHasYadernazbroia.Key == ConsoleKey.Y ? true : false;
                OurCountry.HasChlennato = keyHasChlennato.Key == ConsoleKey.Y ? true : false;
                double YearIncomePerInhabitant = OurCountry.GetYearIncomePerInhabitant();
                arrCountries[i] = OurCountry;
            }
            foreach (Country c in arrCountries)
            {
                Console.WriteLine();
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Данi про об`ект: ");
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Назва: " + c.Name);
                Console.WriteLine("Частина світу: " + c.Region);
                Console.WriteLine("Кiлькiсть населення: " + c.Population.ToString());
                Console.WriteLine("Рiчний дохiд: " + c.YearIncome.ToString("0.00"));
                Console.WriteLine("Площа: " + c.Square.ToString("0.000"));
                Console.WriteLine(c.HasYadernazbroia ? "У країни є ядерна зброя" : "У країни нема ядерної зброї");
                Console.WriteLine(c.HasChlennato ? "Країна є членом НАТО" : "Країна не член НАТО");
                Console.WriteLine();
                Console.WriteLine("Середнiй рiчний дохiд на одного громадянина: " + c.YearIncomePerInhabitant.ToString("0.00"));
            }
            Console.ReadKey();




        }
    }
}
