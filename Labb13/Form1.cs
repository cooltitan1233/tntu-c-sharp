﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labb13
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми",
            MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvCountries.AutoGenerateColumns = false;

            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Region";
            column.Name = "Регіон";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Population";
            column.Name = "Мешканців";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "YearIncome";
            column.Name = "Річн. дохід";
            gvCountries.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Square";
            column.Name = "Площа";
            column.Width = 80;
            gvCountries.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "HasYadernazbroia";
            column.Name = "Ядерна зброя";
            column.Width = 60;
            gvCountries.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "HasChlennato";
            column.Name = "Країна член НАТО";
            column.Width = 60;
            gvCountries.Columns.Add(column);

            bindSrcCountries.Add(new Country("Україна", "Європа", 800000, 2000000, 400, false, true));
            EventArgs args = new EventArgs();
            OnResize(args);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Country country = new Country();
            fCountry ft = new fCountry(country);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcCountries.Add(country);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Country country = (Country)bindSrcCountries.List[bindSrcCountries.Position];
            fCountry ft = new fCountry(country);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcCountries.List[bindSrcCountries.Position] = country;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?",
            "Видалення запису", MessageBoxButtons.OKCancel,
             MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindSrcCountries.RemoveCurrent();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені",
          "Очищення даних", MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindSrcCountries.Clear();
            }
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void btnSaveAsText_Click(object sender, EventArgs e)
        {

            saveFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у текстовому форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            StreamWriter sw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                try
                {
                    foreach (Country country in bindSrcCountries.List)
                    {
                        sw.Write(country.Name + "\t" +
                        country.Region + "\t" + country.Population + "\t" + country.YearIncome + "\t" + country.Square + "\t" +
                        country.HasYadernazbroia + "\t" + country.HasChlennato + "\t\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        private void btnSaveAsBinary_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Файли даних (*.towns)|*.towns|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у бінарному форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            BinaryWriter bw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bw = new BinaryWriter(saveFileDialog.OpenFile());
                try
                {
                    foreach (Country country in bindSrcCountries.List)
                    {
                        bw.Write(country.Name);
                        bw.Write(country.Region); bw.Write(country.Population); bw.Write(country.YearIncome);
                        bw.Write(country.Square);
                        bw.Write(country.HasYadernazbroia);
                        bw.Write(country.HasChlennato);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    bw.Close();
                }
            }
        }

        private void btnOpenFromText_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files(*.*) | *.* ";
            openFileDialog.Title = "Прочитати дані у текстовому форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            StreamReader sr;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcCountries.Clear(); sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                string s;
                try
                {
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('\t');
                        Country country = new Country(split[0], split[1], int.Parse(split[2]), double.Parse(split[3]), double.Parse(split[4]), bool.Parse(split[5]), bool.Parse(split[6]));
                        bindSrcCountries.Add(country);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sr.Close();
                }
            }


        }

        private void btnOpenFromBinary_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Файли даних (*.towns)|*.towns|All files(*.*) | *.* ";
            openFileDialog.Title = "Прочитати дані у бінарному форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            BinaryReader br;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcCountries.Clear();
                br = new BinaryReader(openFileDialog.OpenFile());
                try
                {
                    Country country; while (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        country = new Country();
                        for (int i = 1; i <= 8; i++)
                        {
                            switch (i)
                            {
                                case 1:
                                    country.Name = br.ReadString();
                                    break;
                                case 2:
                                    country.Region = br.ReadString(); break;
                                case 3:
                                    country.Population = br.ReadInt32();
                                    break;
                                case 4:
                                    country.YearIncome = br.ReadDouble();
                                    break;
                                case 5:
                                    country.Square = br.ReadDouble();
                                    break;
                                case 6:
                                    country.HasYadernazbroia = br.ReadBoolean();
                                    break;
                                case 7:
                                    country.HasChlennato = br.ReadBoolean();
                                    break;
                            }
                        }
                        bindSrcCountries.Add(country);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    br.Close();
                }
            }
        }

    }

}
