﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labb13
{
    public class Country
    {
        public string Name { get; set; }
        public string Region { get; set; }
        public int Population { get; set; }
        public double YearIncome { get; set; }
        public double Square { get; set; }
        public bool HasYadernazbroia { get; set; }
        public bool HasChlennato { get; set; }
        public double GetYearIncomePerInhabitant()
        {
            return YearIncome / Population;
        }

        public Country()
        {
        }
        public Country(string name, string region,
        int population, double yearIncome, double square,
        bool hasYadernazbroia, bool hasChlennato)
        {
            Name = name;
            Region = region;
            Population = population; YearIncome = yearIncome;
            Square = square;
            HasYadernazbroia = hasYadernazbroia;
            HasChlennato = hasChlennato;
        }




    }
}
