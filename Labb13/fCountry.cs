﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labb13
{
    public partial class fCountry : Form
    {
        public fCountry()
        {
            InitializeComponent();
        }

        public Country TheCountry;

        public fCountry(Country t)
        {
            TheCountry = t;
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            TheCountry.Name = tbName.Text.Trim();
            TheCountry.Region = tbRegion.Text.Trim(); TheCountry.Population = int.Parse(tbPopulation.Text.Trim()); TheCountry.YearIncome = double.Parse(tbYearIncome.Text.Trim());
            TheCountry.Square = double.Parse(tbSquare.Text.Trim());
            TheCountry.HasYadernazbroia = chbHasYadernazbroia.Checked;
            TheCountry.HasChlennato = chbHasChlennato.Checked;
            DialogResult = DialogResult.OK;
        }

        private void fCountry_Load(object sender, EventArgs e)
        {
            if (TheCountry != null)
            {
                tbName.Text = TheCountry.Name;
                tbRegion.Text = TheCountry.Region;
                tbPopulation.Text = TheCountry.Population.ToString();
                tbYearIncome.Text = TheCountry.YearIncome.ToString("0.00");
                tbSquare.Text = TheCountry.Square.ToString("0.000");
                chbHasYadernazbroia.Checked = TheCountry.HasYadernazbroia; chbHasChlennato.Checked = TheCountry.HasChlennato;
            }
        }
    }
}
