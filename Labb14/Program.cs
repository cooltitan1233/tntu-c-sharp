﻿using Labb14;
using System;
using System.Collections.Generic;
using System.IO;


namespace Labb14
{
    class Program
    {
        static List<Country> countries;
        static void PrintCountries()
        {
            foreach (Country country in countries)
            {
                Console.WriteLine(country.Info().Replace('і', 'i'));
            }
            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            countries = new List<Country>();
            FileStream fs = new FileStream(@"C:\Users\ViVi\Desktop\traning\Labb14\Countries.txt", FileMode.Open);
            BinaryReader reader = new BinaryReader(fs);
            try
            {
                Country country;
                Console.WriteLine(" Читаємо данi з файлу...\n"); while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    country = new Country(); for (int i = 1; i <= 8; i++)
                    {
                        switch (i)
                        {
                            case 1:
                                country.Name = reader.ReadString();
                                break;
                            case 3:
                                country.Region = reader.ReadString();
                                break;
                            case 4:
                                country.Population = reader.ReadInt32();
                                break;
                            case 5:
                                country.YearIncome = reader.ReadDouble();
                                break;
                            case 6:
                                country.Square = reader.ReadDouble();
                                break;
                            case 7:
                                country.HasYadernazbroia = reader.ReadBoolean();
                                break;
                            case 8:
                                country.HasChlennato = reader.ReadBoolean();
                                break;
                        }
                    }
                    countries.Add(country);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Сталась помилка: {0}", ex.Message);
            }
            finally
            {
                reader.Close();
            }
            Console.WriteLine(" Несортований перелiк країн: {0}", countries.Count);
            PrintCountries();
            countries.Sort();
            Console.WriteLine(" Сортований перелiк країн: {0}", countries.Count);
            PrintCountries();
            Console.WriteLine(" Додаємо новий запис: Україна");
            Country countryYkrain = new Country("Україна", "Центральний", 997000,
             4000000, 237, true, true);
            countries.Add(countryYkrain);
            countries.Sort();
            Console.WriteLine(" Перелiк країн: {0}", countries.Count);
            PrintCountries();
            Console.WriteLine(" Видаляємо останнє значення");
            countries.RemoveAt(countries.Count - 1);
            Console.WriteLine(" Перелiк країн: {0}", countries.Count);
            PrintCountries();
            Console.ReadKey();

        }
    }
}
